﻿using System;
using System.Runtime.InteropServices;
//using HardwareDetector;
//using SoftwareDetector;

namespace DotNetCoreConsole
{
    class Program
    {
        /// <summary>
        /// GUI library name for current platform;
        /// </summary>
        private const string GUI_LIB = "QtVsTGUILib.dll";
        /// <summary>
        /// Start the GUI library;
        /// </summary>
        /// <param name="argc">argument count</param>
        /// <param name="argv">argument value</param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport(GUI_LIB)]
        public static extern int StartUp(ref int argc, string[] argv, CommandCallback testCallback);
        /// <summary>
        /// Stop the GUI library;
        /// </summary>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport(GUI_LIB)]
        public static extern int ShutDown();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataLen"></param>
        /// <param name="data"></param>
        /// <param name="testCallback"></param>
        /// <returns></returns>
        [System.Runtime.InteropServices.DllImport(GUI_LIB)]
        public static extern int SetModelData(ref int dataLen, string[] data);

        /// <summary>
        /// Test callback;
        /// </summary>
        /// <param name="value"></param>
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void CommandCallback(int value);

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Hello World & Welcome to the HARMAN VTB Descriptor!" + Environment.NewLine + "\tPress any key to continue.");
                Console.ReadKey(true);
                Console.WriteLine("Supported commands: " + Environment.NewLine +
                    //"\t" + " - HW: list all connected ECUs;" + Environment.NewLine +
                    //"\t" + " - SW: list all installed software;" + Environment.NewLine +
                    "\t" + " - GUI: starts the GUI app;" + Environment.NewLine +

                    "----" + Environment.NewLine +
                    "\t" + " - EXIT: exit application." + Environment.NewLine +
                    "=========================================================" + Environment.NewLine +
                    "All commands are case insensitive." + Environment.NewLine +
                    "=========================================================");

                string input = string.Empty;
                while (input.ToLowerInvariant() != "exit")
                {
                    input = Console.ReadLine();
                    switch (input.ToLowerInvariant())
                    {
                        //case "hw":
                        //    DetectHardware();
                        //    break;
                        //case "sw":
                        //    DetectSofware();
                        //    break;
                        case "gui":
                            if (System.IO.File.Exists(System.IO.Path.Combine(AppContext.BaseDirectory, GUI_LIB)))
                            {
                                Console.WriteLine("Found!");
                                StartGuiApp(args.Length, args);
                            }
                            else
                                Console.WriteLine("!Found!!!@: " + System.IO.Path.Combine(AppContext.BaseDirectory, GUI_LIB));

                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey(true);
            }
        }
        static void DetectHardware()
        {
            Console.WriteLine("Detecting HW");
            //HardwareDetector.HardwareDetector hardwareDetector = new HardwareDetector.HardwareDetector();
            //hardwareDetector.Init(new dummyReportObj());
        }
        static void DetectSofware()
        {
            Console.WriteLine("Detecting SW");
            //SoftwareDetector.SoftwareDetector softwareDetector = new SoftwareDetector.SoftwareDetector(new dummyReportObj());
            //softwareDetector.Scan();
        }
        static void StartGuiApp(int count, string[] values)
        {
            CommandCallback testCallback = (value) =>
            {
                Console.Write("Progress = {0}", value);
                switch (value)
                {
                    case 0:
                        string[] args = new string[] { "unu", "doi", "trei", "patru", "cinci" };
                        int argc = args.Length;
                        SetModelData(ref argc, args);
                        break;
                }
            };

            StartUp(ref count, values, testCallback);
        }
        static void StopGuiApp()
        {
            ShutDown();
        }
    }

    class dummyReportObj
    {

    }
}
