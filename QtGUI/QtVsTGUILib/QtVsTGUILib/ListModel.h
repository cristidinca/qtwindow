#pragma once

#ifndef LIST_MODEL_H
#define LIST_MODEL_H

#include <QAbstractListModel>

class ListModel : public QAbstractListModel
{
	Q_OBJECT

public:
	ListModel(QObject *parent);
	~ListModel();

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
	//QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	void setData(const int & dataLen, const char ** data, int role = Qt::EditRole);
private:
	
	QList<QString> _data;
};
#endif // !LIST_MODEL_H
