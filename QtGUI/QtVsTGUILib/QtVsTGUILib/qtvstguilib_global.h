#pragma once
#ifndef QT_GUI_GLOBAL_H
#define QT_GUI_GLOBAL_H

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(QT_GUI_GLOBAL_H)
#  define QTGUI_EXPORT Q_DECL_EXPORT
typedef void(__stdcall * CommandCallback)(int);
# else
#  define QTGUILIB_EXPORT Q_DECL_IMPORT
# endif
#else
# define QTGUILIB_EXPORT
#endif

#endif // !QT_GUI_GLOBAL_H
