#include "QtApp.h"

QtApp::QtApp(int & argc, char ** argv) : QApplication(argc, argv), model(nullptr)
{
	mainWindow = new QMainWindow(nullptr);
	mainWidget = new QWidget();
	listView = new QListView(nullptr);
	button = new QPushButton(mainWidget);
	vBoxLayout = new QVBoxLayout();

	model = new ListModel(this);
	viewModel = new ListViewModel(nullptr);
}

QtApp::~QtApp()
{
}

void QtApp::Run(CommandCallback callback)
{
	button->setText("Software & stuff");
	QObject::connect(button, &QPushButton::released, [=] () { viewModel->HandleButtonPress(callback); });
	//callback(42);
	listView->setModel(model);
	//callback(42);
	vBoxLayout->addWidget(button);
	vBoxLayout->addWidget(listView);
	mainWidget->setLayout(vBoxLayout);

	mainWindow->setCentralWidget(mainWidget);
	mainWindow->show();
}

void QtApp::SetModel(const int & argc, const char ** argv)
{
	model->setData(argc, argv, Qt::EditRole);
}

