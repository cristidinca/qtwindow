#include "QtLib.h"

QtLib * gui;
QtApp * gApp;

QtLib::QtLib(int & argc, char **argv, CommandCallback callback)
{
	gApp = new QtApp(argc, argv);
	app = gApp;
	app->Run(callback);
	app->exec();
}

QtLib::~QtLib()
{
}

QTGUI_EXPORT void StartUp(int & argc, char **argv, CommandCallback commandCallback)
{
	gui = new QtLib(argc, argv, commandCallback);
	gui->deleteLater();
	gApp->deleteLater();
}

QTGUI_EXPORT void ShutDown()
{
	gui = nullptr;
	delete gui;
}

QTGUI_EXPORT void SetModelData(const int & dataLen, const char ** data)
{
	gApp->SetModel(dataLen, data);
	//gui->app->SetModel(argc, argv);	/*Crashes*/
}
