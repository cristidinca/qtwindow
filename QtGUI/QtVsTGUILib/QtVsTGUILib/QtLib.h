#pragma once

#ifndef QTLIB_H
#define QTLIB_H

#include "qtvstguilib_global.h"
#include "QtApp.h"

#include <QThread>
#include <QModelIndex> 
#include <QVariant> 

static class QTGUI_EXPORT QtLib : public QThread
{
	Q_OBJECT
public:
	QtLib(int & argc, char ** argv, CommandCallback callback);
	~QtLib();

	QtApp * app;
};

extern "C" QTGUI_EXPORT void StartUp(int & argc, char ** argv, CommandCallback commandCallback);
extern "C" QTGUI_EXPORT void ShutDown();

extern "C" QTGUI_EXPORT void SetModelData(const int & dataLen, const char ** data);

#endif // !QTLIB_H
