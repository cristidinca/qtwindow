#include "ListModel.h"

ListModel::ListModel(QObject *parent) : QAbstractListModel(parent)
{
	for (int i = 0; i < 32; i++)
		_data << "Ce kkt: " << QString::number(i);
}

ListModel::~ListModel()
{
}

int ListModel::rowCount(const QModelIndex & parent) const
{
	Q_UNUSED(parent);
	return _data.count();
}

QVariant ListModel::data(const QModelIndex & index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (index.row() > _data.count() || index.row() < 0)
		return QVariant();

	if (role == Qt::DisplayRole)
		return _data[index.row()];

	return QVariant();
}

bool ListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.row() != _data.count())
			_data.reserve(index.row());

		//_data.replace(index.row(), QString(value.toString()));
		_data[index.row()] = QString(value.toString());

		emit dataChanged(index, index, {role});

		return true;
	}

	return false;
}

void ListModel::setData(const int & dataLen, const char ** data, int role)
{
	if (role == Qt::EditRole)
	{
		_data.clear();
		for (int i = 0; i < dataLen; i++) 
		{
			_data << QString::fromStdString(data[i]);
			const QModelIndex index = this->index(dataLen, 0, QModelIndex());
			const QVariant value = QVariant(QString::fromStdString(data[i]));
			emit dataChanged(index, index, { role });
		}
	}
}


