#pragma once

#ifndef LIST_VIEW_MODEL_H
#define LIST_VIEW_MODEL_H

#include <QObject>

#include "qtvstguilib_global.h"

class ListViewModel : public QObject
{
	Q_OBJECT

public:
	ListViewModel(QObject *parent);
	~ListViewModel();

	void HandleButtonPress(CommandCallback callback);
};
#endif // !LIST_VIEW_MODEL_H
