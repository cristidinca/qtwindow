#pragma once

#ifndef QTT_APP_H
#define QTT_APP_H

#include <QApplication>
#include <QMainWindow> 
#include <QListView> 
#include <QPushButton> 
#include <QStackedLayout> 

#include "ListModel.h"
#include "ListViewModel.h"

static class QtApp : public QApplication
{
	Q_OBJECT

public:
	QtApp(int & argc, char ** argv);
	~QtApp();

	void Run(CommandCallback callback);
	void SetModel(const int & dataLen, const char ** data);

private:
	QMainWindow *mainWindow;
	QWidget *mainWidget;
	QListView *listView;
	QPushButton *button;
	QVBoxLayout *vBoxLayout;

	ListModel *model;
	ListViewModel *viewModel;
};

#endif // !QTT_APP_H
